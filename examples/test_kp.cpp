#include <iostream>
#include <string>
#include <cassert>
#include <openabe/openabe.h>
#include <openabe/zsymcrypto.h>

using namespace std;
using namespace oabe;
using namespace oabe::crypto;

int main(int argc, char **argv) {
    cout << "Hello, Welcome to ABE test lab :)" << endl;
    cout << "######### Testing KP-ABE context #########" << endl;
    cout << "Let's do some work with KP-ABE..." << endl << endl;

    cout << "1. Initializing Open ABE" << endl;
    InitializeOpenABE();

    cout << "2. Instantiating a crypto box KP-ABE context" << endl;
    // instantiate a crypto box KP-ABE context
    OpenABECryptoContext kpabe("KP-ABE");

    cout << "3. Generating Public and Secret Parameters" << endl;
    // generate a fresh master public and master secret parameters
    kpabe.generateParams();

    // export master public params
    std::string mpk;
    kpabe.exportPublicParams(mpk);
    cout << "Generated Public Parameters is=> " << endl << mpk << endl << endl;

    // export master secret params
    std::string msk;
    kpabe.exportSecretParams(msk);
    cout << "Generated Master Secret Key is (NOTE: should be secret :|)=> " << endl << mpk << endl << endl;

    // We can load public params and master secret key from disc, DB,... as follow (NOT necessary here!)
    // load master public params
    kpabe.importPublicParams(mpk);
    // load master secret params
    kpabe.importSecretParams(msk);


    cout << "4. Generating secret key with 'attr1 and attr2' policy (NOTE: In KP-ABE 'access policy' should be pass instead of 'list of attrs' in secret key generating!)" << endl;
    // generate a new key with the following policy
    kpabe.keygen("attr1 and attr2", "key0");

    // export user secret key
    std::string key0Blob;
    kpabe.exportUserKey("key0", key0Blob);
    cout << "Generated Key is (NOTE: should be secret:|)=> " << endl << key0Blob << endl << endl;

    // We also can import key from disc, DB with "importUserKey"
    kpabe.importUserKey("key0", key0Blob);

    string ct, pt1 = "Hello to KP-ABE by Mohammad Mahdi Mojahed :)", pt2;

    cout << "5. Encrypting Message with attr list '|attr1|attr2' as input" << endl;
    // encrypt a message with the following list of attrs
    kpabe.encrypt("|attr1|attr2", pt1, ct);
    cout << "Encrypted Message is=> " << endl << ct << endl << endl;

    cout << "6. Decrypting Message with appropriate key" << endl;
    // decrypt message with appropriate key
    bool result = kpabe.decrypt("key0", ct, pt2);

    // check if decrypt was successful and plain-texts recovered
    assert(result && pt1 == pt2);

    cout << "Decrypted Message is=> " << endl << pt2 << endl << endl;

    cout << "7. Shutting down Open ABE, GoodBy :)" << endl;
    ShutdownOpenABE();

    return 0;
}
