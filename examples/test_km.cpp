#include <iostream>
#include <string>
#include <cassert>
#include <map>
#include <vector>
#include <openabe/openabe.h>
#include <openabe/zsymcrypto.h>

using namespace std;
using namespace oabe;
using namespace oabe::crypto;

int main(int argc, char **argv) {

    cout << "Hello, Welcome to ABE test lab :)" << endl;
    cout << "######### Testing KP-ABE context with Key Manager #########" << endl;
    cout << "Let's do some work with KP-ABE Key Manager..." << endl << endl;

    cout << "1. Initializing Open ABE" << endl;
    InitializeOpenABE();

    cout << "2. Instantiating a crypto box KP-ABE context" << endl;
    // instantiate a crypto box KP-ABE context
    OpenABECryptoContext kpabe("KP-ABE");

    cout << "3. Generating Public and Secret Parameters" << endl;
    // generate a fresh master public and master secret parameters
    kpabe.generateParams();

    cout << "4. Generating keys and deleting from context (We will load later)" << endl;
    map <string, string> keyBlobs;
    string tmp;
    vector <string> key_inputs = {"(attr1 or attr2) and attr3", "attr1 and attr2", "attr2 and attr3",
                                  "attr3 and attr4"};
    // generate keys and delete from context (we will load later)
    for (size_t i = 0; i < key_inputs.size(); i++) {
        const string keyID = "key" + to_string(i + 1);
        cout << "Generate " << keyID << ": " << key_inputs[i] << endl;
        kpabe.keygen(key_inputs[i], keyID);
        kpabe.exportUserKey(keyID, tmp);
        keyBlobs[keyID] = tmp;
        assert(kpabe.deleteKey(keyID));
    }

    cout << "5. Enabling use of key manager feature" << endl;
    // enable use of key manager
    kpabe.enableKeyManager("user1");
    kpabe.enableVerbose(); // more internal output to stdout

    cout << "6. Load the keystore with the generated keys" << endl;
    map<string, string>::iterator it;
    // load the keystore with the generated keys
    for (it = keyBlobs.begin(); it != keyBlobs.end(); it++) {
        kpabe.importUserKey(it->first, it->second);
    }

    string pt1 = "hello world!", pt2 = "another hello!", pt3 = "this should fail!";
    string rpt1, rpt2, rpt3, ct1, ct2, ct3;

    cout << "7. Encrypting messages with different attr list" << endl;
    // encrypt
    kpabe.encrypt("|attr1|attr2", pt1, ct1);
    kpabe.encrypt("|attr3|attr4", pt2, ct2);
    kpabe.encrypt("|attr4|attr5", pt3, ct3);

    cout << "8. Decrypting messages with different scenario as follow" << endl;
    // decrypt
    bool result = kpabe.decrypt(ct1, rpt1);

    assert(result && pt1 == rpt1);
    cout << "Recovered message 1: " << rpt1 << endl;

    result = kpabe.decrypt(ct2, rpt2);
    assert(result && pt2 == rpt2);
    cout << "Recovered message 2: " << rpt2 << endl;

    try {
        result = kpabe.decrypt(ct3, rpt3);
    } catch (oabe::ZCryptoBoxException &ex) {
        cout << "Correctly failed to recover message 3!" << endl;
    }

    cout << "9. Shutting down Open ABE, GoodBy :)" << endl;
    ShutdownOpenABE();

    return 0;
}
