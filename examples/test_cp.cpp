/// 
/// Copyright (c) 2018 Zeutro, LLC. All rights reserved.
/// 
/// This file is part of Zeutro's OpenABE.
/// 
/// OpenABE is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// 
/// OpenABE is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// 
/// You should have received a copy of the GNU Affero General Public
/// License along with OpenABE. If not, see <http://www.gnu.org/licenses/>.
/// 
/// You can be released from the requirements of the GNU Affero General
/// Public License and obtain additional features by purchasing a
/// commercial license. Buying such a license is mandatory if you
/// engage in commercial activities involving OpenABE that do not
/// comply with the open source requirements of the GNU Affero General
/// Public License. For more information on commerical licenses,
/// visit <http://www.zeutro.com>.
///
/// \brief  Example use of the OpenABE API with CP-ABE
///

#include <iostream>
#include <string>
#include <cassert>
#include <openabe/openabe.h>
#include <openabe/zsymcrypto.h>

using namespace std;
using namespace oabe;
using namespace oabe::crypto;

int main(int argc, char **argv) {
    cout << "Hello, Welcome to ABE test lab :)" << endl;
    cout << "######### Testing CP-ABE context #########" << endl;
    cout << "Let's do some work with CP-ABE..." << endl << endl;

    cout << "1. Initializing Open ABE" << endl;
    InitializeOpenABE();

    cout << "2. Instantiating a crypto box CP-ABE context" << endl;
    // instantiate a crypto box CP-ABE context
    OpenABECryptoContext cpabe("CP-ABE");

    cout << "3. Generating Public and Secret Parameters" << endl;
    // generate a fresh master public and master secret parameters
    cpabe.generateParams();

    // export master public params
    std::string mpk;
    cpabe.exportPublicParams(mpk);
    cout << "Generated Public Parameters is=> " << endl << mpk << endl << endl;

    // export master secret params
    std::string msk;
    cpabe.exportSecretParams(msk);
    cout << "Generated Master Secret Key is (NOTE: should be secret :|)=> " << endl << mpk << endl << endl;

    // We can load public params and master secret key from disc, DB,... as follow (NOT necessary here!)
    // load master public params
    cpabe.importPublicParams(mpk);
    // load master secret params
    cpabe.importSecretParams(msk);

    cout
            << "4. Generating secret key with '|attr1|attr2' list (NOTE: In CP-ABE 'list of attrs' should be pass instead of 'access policy' in secret key generating!)"
            << endl;
    // generate a new key with the following policy
    cpabe.keygen("|attr1|attr2", "key0");

    // export user secret key
    std::string key0Blob;
    cpabe.exportUserKey("key0", key0Blob);
    cout << "Generated Key is (NOTE: should be secret:|)=> " << endl << key0Blob << endl << endl;

    // We also can import key from disc, DB with "importUserKey"
    cpabe.importUserKey("key0", key0Blob);

    string ct, pt1 = "Hello to CP-ABE by Mohammad Mahdi Mojahed :)", pt2;

    cout << "5. Encrypting Message with policy 'attr1 and attr2' as input" << endl;
    // encrypt a message with the following policy
    cpabe.encrypt("attr1 and attr2", pt1, ct);
    cout << "Encrypted Message is=> " << endl << ct << endl << endl;

    cout << "6. Decrypting Message with appropriate key" << endl;
    // decrypt message with appropriate key
    bool result = cpabe.decrypt("key0", ct, pt2);

    // check if decrypt was successful and plain-texts recovered
    assert(result && pt1 == pt2);

    cout << "Decrypted Message is=> " << endl << pt2 << endl << endl;

    cout << "7. Shutting down Open ABE, GoodBy :)" << endl;
    ShutdownOpenABE();

    return 0;
}
